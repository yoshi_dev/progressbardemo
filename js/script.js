(function () {
    const ENDPOINTS = {
        "buttons": [10, 38, 32, 22, 122, -13, -23, -43, -18],
        "bars": [62, 45, 10, 21, 211, 93, 74],
        "limit": 230
    };

    const HUNDRED = 100, ZERO = 0;
    const COLORS = { start: 'aqua', end: 'white', overflow: 'red' };

    function getLinear(percentage) {
        let startColor = COLORS.start;
        if (percentage > HUNDRED) {
            percentage = HUNDRED;
            startColor = COLORS.overflow;
        } else if (percentage < ZERO) {
            percentage = ZERO;
        }
        return 'linear-gradient(to right, ' + startColor + ' '
            + percentage + '%, ' + COLORS.end + ' ' + percentage + '%)';
    }

    function getNewProgress(p) {
        if (p > ENDPOINTS.limit)
            return ENDPOINTS.limit;
        if (p < ZERO)
            return ZERO;
        return p;
    }

    function makeProgress(n, i) {
        let progressDiv = document.getElementsByName('progress-bars')[ZERO]
            .getElementsByTagName('DIV')[i];
        ENDPOINTS.bars[i] += n;
        ENDPOINTS.bars[i] = getNewProgress(ENDPOINTS.bars[i]);
        progressDiv.style.background = getLinear(ENDPOINTS.bars[i]);
        progressDiv.childNodes[ZERO].innerHTML = ENDPOINTS.bars[i] + "%";
    }

    function renderProgressBars() {
        let innerContent = document.getElementsByName('progress-bars')[ZERO];
        for (let bar in ENDPOINTS.bars) {
            let div = document.createElement('DIV');
            let span = document.createElement('SPAN');
            div.className += " bar-one progress-bar";
            div.style.background = getLinear(ENDPOINTS.bars[bar]);
            span.className += " percentage";
            span.innerHTML = ENDPOINTS.bars[bar] + "%";
            div.appendChild(span);
            innerContent.appendChild(div);
        }
    }

    function renderButtons(btns, select) {
        for (let i in ENDPOINTS.buttons) {
            let button = document.createElement('BUTTON');
            button.innerHTML = ENDPOINTS.buttons[i];
            button.className += " progress-btn";
            button.onclick = function () {
                makeProgress(ENDPOINTS.buttons[i], select.selectedIndex);
            };
            btns.appendChild(button);
        }
    }

    function renderSelect(btns, select) {
        for (let bar in ENDPOINTS.bars) {
            let option = document.createElement('OPTION');
            option.innerHTML = "Progress " + (Number(bar) + 1);
            select.appendChild(option);
        }
        btns.appendChild(select);
    }

    function render() {
        let select = document.createElement('SELECT');
        let btns = document.getElementsByClassName('btns')[ZERO];
        renderSelect(btns, select);
        renderButtons(btns, select);
        renderProgressBars();
    }

    window.onload = render;
})();